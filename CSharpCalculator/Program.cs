﻿using CSharpCalculator.Implementations;
using CSharpCalculator.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpCalculator
{
	class Program
	{
		static void Main(string[] args)
		{
			InputService iServ = new InputService();
			ValidationService vServ = new ValidationService();
			OperationService oServ = new OperationService();
			DisplayService dServ = new DisplayService();

			Calculator calc = new Calculator(iServ, oServ, vServ, dServ);
			calc.Calculate();
			Console.ReadKey();
		}
	}
}
