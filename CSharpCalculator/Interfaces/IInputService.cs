﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpCalculator.Interfaces
{
	interface IInputService
	{
		string ReadFirstNumber();
		string ReadSecondNumber();
		string ReadOperation();
	}
}
