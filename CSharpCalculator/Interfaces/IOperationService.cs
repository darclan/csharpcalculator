﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpCalculator.Interfaces
{
	interface IOperationService
	{
		string PerformOperation(string firstNumber, string secondNumber, string operation);
	}
}
