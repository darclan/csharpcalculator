﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpCalculator.Interfaces
{
	interface IValidationService
	{
		string Validate(string firstNumber, string secondNumber, string operation);
	}
}
