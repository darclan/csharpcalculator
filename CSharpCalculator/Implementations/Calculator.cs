﻿using CSharpCalculator.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpCalculator.Implementations
{
	class Calculator
	{
		protected IInputService inputService;
		protected IOperationService operationService;
		protected IValidationService validationService;
		protected IDisplayService displayService;

		public Calculator(IInputService inputService, IOperationService operationService, IValidationService validationService, IDisplayService displayService)
		{
			this.inputService = inputService;
			this.operationService = operationService;
			this.validationService = validationService;
			this.displayService = displayService;
		}
		public void Calculate()
		{
			//read first number
			string firstNumber = this.inputService.ReadFirstNumber();
			//read second number
			string secondNumber = this.inputService.ReadSecondNumber();
			//read operation
			string operation = this.inputService.ReadOperation();
			//validation
			string errorMessage = this.validationService.Validate(firstNumber, secondNumber, operation);

			if (String.IsNullOrEmpty(errorMessage))
			{
				//perform operation
				string result = this.operationService.PerformOperation(firstNumber, secondNumber, operation);
				//display result
				this.displayService.Display(result);
			}
			else
			{
				//display error message
				this.displayService.Display(errorMessage);
			}
		}
	}
}

