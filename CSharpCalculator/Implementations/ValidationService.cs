﻿using CSharpCalculator.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpCalculator.Implementations
{
	class ValidationService : IValidationService
	{
		public string Validate(string firstNumber, string secondNumber, string operation)
		{
			string ErrorMessage;
			ErrorMessage = CheckOperationSign(operation);
			ErrorMessage = ErrorMessage+CheckForNotNumbers(firstNumber);
			ErrorMessage = ErrorMessage+CheckForNotNumbers(secondNumber);
			ErrorMessage = ErrorMessage+CheckForInvalidDivision(secondNumber, operation);
			return ErrorMessage;
		}

		string CheckOperationSign(string operation)
		{
			if (!((operation == "+") || (operation == "-") || (operation == "*") || (operation == "/")))
			{
				return "Illegal operation sign!";
			}
			else
			{
				return null;
			}
		}

		string CheckForNotNumbers(string suspectedNumber)
		{
			decimal result;
			if (Decimal.TryParse(suspectedNumber, out result))
			{
				return null;
			}
			else
			{
				return "Input is not a number!";
			}
		}

		string CheckForInvalidDivision(string secondNumber, string operation)
		{
			if ((operation == "/") && (ConvertNumber(secondNumber) == 0))
			{
				return "Cannot attempt to divide by 0!";
			}
			else
			{
				return null;
			}
		}

		decimal ConvertNumber(string number)
		{
			return Decimal.Parse(number);
		}
	}
}
