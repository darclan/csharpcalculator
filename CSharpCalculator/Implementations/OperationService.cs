﻿using CSharpCalculator.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpCalculator.Implementations
{
	class OperationService : IOperationService
	{
		public string PerformOperation(string firstNumber, string secondNumber, string operation)
		{
			decimal result = 0;

			switch (operation)
			{
				case "+":
					result = ConvertNumber(firstNumber) + ConvertNumber(secondNumber);
					break;
				case "-":
					result = ConvertNumber(firstNumber) - ConvertNumber(secondNumber);
					break;
				case "*":
					result = ConvertNumber(firstNumber) * ConvertNumber(secondNumber);
					break;
				case "/":
					result = ConvertNumber(firstNumber) / ConvertNumber(secondNumber);
					break;
				default:
					return "Invalid operation sign provided";
			}
			return result.ToString();
		}

		decimal ConvertNumber(string number)
		{
			return Decimal.Parse(number);
		}
	}
}
