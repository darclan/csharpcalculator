﻿using CSharpCalculator.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpCalculator.Implementations
{
	class InputService : IInputService
	{
		public string ReadFirstNumber()
		{
			Console.Write("Provide first number: ");
			return Console.ReadLine();
		}

		public string ReadOperation()
		{
			Console.Write("Provide operation: ");
			return Console.ReadLine();
		}

		public string ReadSecondNumber()
		{
			Console.Write("Provide second number: ");
			return Console.ReadLine();
		}
	}
}
