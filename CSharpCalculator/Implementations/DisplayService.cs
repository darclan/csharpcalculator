﻿using CSharpCalculator.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpCalculator.Implementations
{
	class DisplayService : IDisplayService
	{
		public void Display(string message)
		{
			Console.WriteLine("The result is: "+message);
		}
	}
}
